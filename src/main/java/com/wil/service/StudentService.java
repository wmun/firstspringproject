package com.wil.service;

import com.wil.dao.StudentDao;
import com.wil.entity.Student;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    @Autowired
    private StudentDao studentDao;

    public Collection<Student> getAllStudents() {
        return studentDao.getAllStudents();
    }

    public Student getStudentById(int id) {
        return studentDao.getStudentById(id);
    }

    public void removeStudentById(int id) {
        studentDao.removeStudentById(id);
    }

    public void updateStudentById(Student student) {
        studentDao.updateStudentById(student);
    }

    public void insertStudent(Student student) {
        studentDao.insertStudent(student);
    }
}
